package main

import (
	p "dbatch/process"
)

func main() {
	data := make([]int, 0, 100)
	for n := 0; n < 100; n++ {
		data = append(data, n)
	}
	//process(data)
	p.Process(data)
}
