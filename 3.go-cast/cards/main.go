package main

import "fmt"

/*
func main() {
	card := newCard()
	fmt.Println(card)
}*/

func main() {
	cards := []string{"Ace of Diamonds", newCard(), newCard()}
	cards = append(cards, "Six of Spades")
	//fmt.Println(cards)

	for i, card := range cards {
		fmt.Println(i, card)
	}
}

func newCard() string {
	return "Five of Diamonds"
}

/*
func newCard() int {
	return 12
}*/
