# WebService
* [Creating a web server with Golang](https://blog.logrocket.com/creating-a-web-server-with-golang/)


#   Run
```bash
go run .
```

# Testing ws app
* [http://localhost:8081](http://localhost:8081)
* [http://localhost:8081/hello](http://localhost:8081/hello)
* [http://localhost:8081/form?name=leo&address=calle123](http://localhost:8081/form?name=leo&address=calle123)

# Build
```bash
go build
```
