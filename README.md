# kimun-go
Go (golang) is well suited to writing serverless applications because it has a modern set of libraries covering everything from Marshaling to HTTP to Image Manipulation and much more. This results in concise code which is easy to understand and also offers a simple API for concurrency through channels.

Your code can compile down to a small binary with no other external runtimes required - it can offer C-like speed and power but with type-safety.

We'll start with hello world then move onto a function which makes use of a third-party (vendored) library to create hashes from Go structs.

* [Go Lang project example](https://gitlab.com/l30bravo/golang-example)

## Installing Go
```bash
apt install golang-go
apt install go-dep
apt install gom
```

## Run the code
Compiles and executes one or two files
```bash
go run .
```

## Installing dependencies
```bash
go get -u {repository}
```

for example:
```bash
go get github.com/mattn/go-sqlite3
go get -u gorm.io/gorm
go get -u github.com/spf13/viper
```

## Compile
```bash
go build
go build main.go
```

## Other commands
* `go fmt` - formats all the code in each file in the current directory
* `go install` - Compiles and "installs" a package (like mvn)
* `go get` -  download the raw source code of someone else's package
* `o test` - Runs any tests associated with the current project

## Dep [deprecated]
`dep` is a dependency management tool for Go.
* [dep](https://github.com/golang/dep)


## Code
* [The Go Playground](https://play.golang.org/)

* `package` : package == project == workspace, common source files
![package](img/pkg.png)
why main?, exist two types of package, executable and reusable
- `executable` - generates a file that we can run (`package main`).
![main package](img/main.png)
- `reusable` - code used as `helpers` functions. Good place to put reusable logic.
![reusable package](img/reusable-pkg.png)

* `import` : access to some code written in another package (standard lib or reusable package).
- [list of standard pkg](https://golang.org/pkg/)

* variables
![Variables](img/variables.png)
![Var Types](img/var-types.png)

* function
![function structure](img/func.png)

* `nil` - nil in Go is simply the NULL pointer value of other languages.
In Go, nil is the zero value for pointers, interfaces, maps, slices, channels and function types, representing an uninitialized value.
nil doesn't mean some "undefined" state, it's a proper value in itself. An object in Go is nil simply if and only if it's value is nil, which it can only be if it's of one of the aforementioned types.



* [Create module](https://golang.org/doc/tutorial/create-module)
```go
package greetings

import "fmt"

// Hello returns a greeting for the named person.
func Hello(name string) string {
    // Return a greeting that embeds the name in a message.
    message := fmt.Sprintf("Hi, %v. Welcome!", name)
    return message
}
```

`func` + `name` + `(var tye-var)` + 'var-output'

## for
```go
cards := []string{"card1", "card2"}
cards = append(cards, "card3")
for i, card := range cards {
  fmt.Println(i, card)
}
```

## adding method to one variable
```go
package main

import "fmt"

func main() {
   c := color("Red")

   fmt.Println(c.describe("is an awesome color"))
}

type color string

func (c color) describe(description string) (string) {
   return string(c) + " " + description
}
```

## Arrays
![Array and Slice](img/array.png)
- `àrray` - Fixed length list of things.
- `slice` - An array that can grow or shrink. (like vector un c++).
Every element in a slice must be of same type
![slice type](img/slice.png)
one example to iterate one array or slice:
![iterate](img/iterate.png)

* example1:
```go
type arr []string
```
* example2:
```go
arr2 := []string{"uno", "dos", "tres", "cuatro", "cinco", "seis"}
```
* split
![split array example 1](img/array2.png)
or
![split array example 2](img/array3.png)

## byte slice
![byte slice example](img/bite-slice.png)
!![byte slice example2](img/byte-slice2.png)
* for example:
```go
text := "hola mundo"
textBytesSlice := []byte(text)
```
## Write a file

```go
type deck []string

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}
```
## Read a file
```go
type deck []string

func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		// Option #1 - log the error and return a call to newDeck()
		// Option #2 - Log the error and entirely quit the program
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	s := strings.Split(string(bs), ",")
	return deck(s)
}
```

## Random Numbers
```go
type deck []string
func (d deck) shuffle() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
```

## Unit Test
* Unit Testing in Go – testing package
```go
import “testing”

func TestConvert(t *testing.T) {
    t.Log(“Hello World”)
    t.Fail()
}
```
* To test a program, first we’ll need a program, so let’s do that quickly. We’ll create conv.go that will take as input distance values and convert it to other specified formats.

```go
package main

import (
    “errors”
    “fmt”
    “strconv”
    “strings”
)

const (
    kilometersToMiles = 0.621371

    milesToKilometres = 1.60934
    )

func Convert (from, to string) (string,error) {
    var result float64
    switch {
    case strings.HasSuffix(from,”mi”):
        miles,err := strconv.ParseFloat(from[:len(from)-2],64)
        if err != nil {
            return “”,err
            }



    switch to {
    case “km”:
        result = miles * milesToKilometres
    case “m”:
        result = miles * milesToKilometres * 1000
    case “mi”:
        result = miles
    }
}
```

# Go Rutines - Example 1

A goroutine is a lightweight thread of execution. [link](https://gobyexample.com/goroutines)
```go
package main

import (
    "fmt"
    "time"
)

func f(from string) {
    for i := 0; i < 3; i++ {
        fmt.Println(from, ":", i)
    }
}

func main() {

    f("direct")

    go f("goroutine")

    go func(msg string) {
        fmt.Println(msg)
    }("going")

    time.Sleep(time.Second)
    fmt.Println("done")
}
```

# Go Rutines - Example 2
```go
 package main
import (
   "fmt"
   "sync"
)
func check(count int, wg *sync.WaitGroup) {
   defer wg.Done()
   fmt.Println("Inside check", count)
}
func main() {
   count := 3
   var wg sync.WaitGroup
   wg.Add(count)
   for i := 1; i <= count; i++ {
      go check(i, &wg)
   }
   wg.Wait()
   fmt.Println("Done")
}
```

# Links
* [Documentation](https://golang.org/doc/)
* [Go: The Complete developers guide (golang)](https://www.udemy.com/course/go-the-complete-developers-guide/)
* [GoCasts](https://github.com/StephenGrider/GoCasts)
* [Creating a web server with Golang](https://blog.logrocket.com/creating-a-web-server-with-golang/)
* [Go Simple HTTP file upload tutorial](https://youtu.be/0sRjYzL_oYs)
* [Go write file](https://zetcode.com/golang/writefile/)
* [Go + PostgreSQL](https://www.calhoun.io/connecting-to-a-postgresql-database-with-gos-database-sql-package/)
* [ORM Grom.io](https://gorm.io/docs/create.html)
* [query for a single record using sqlite3](https://www.calhoun.io/querying-for-a-single-record-using-gos-database-sql-package/)
* [golang sqlite simple example](https://johnpili.com/golang-sqlite-simple-example/)
* [understanding init in go es](https://www.digitalocean.com/community/tutorials/understanding-init-in-go-es))
* [The fantastic ORM library for Golang](https://gorm.io/index.html)
* [GORM Guides](https://gorm.io/docs/index.html)
* [Load config from file & environment variables in Golang with Viper](https://dev.to/techschoolguru/load-config-from-file-environment-variables-in-golang-with-viper-2j2d)
* [Viper](https://github.com/spf13/viper)
* [GoLang Logging](https://www.honeybadger.io/blog/golang-logging/)
* [Standard Go Project Layout](https://github.com/golang-standards/project-layout)
* [Docker Env vars](https://vsupalov.com/docker-env-vars/)
* [cmd/go: go get package@branch fails after upgrading to 1.13 #34175](https://github.com/golang/go/issues/34175)
* [cmd/go: 'go get' fails to track a branch which contains a `/` #38861](https://github.com/golang/go/issues/38861)
* [410 gone during go.mod import #35164](https://github.com/golang/go/issues/35164)
* [Fix: go get private repository return error reading sum.golang.org/lookup … 410 gone](https://medium.com/mabar/today-i-learned-fix-go-get-private-repository-return-error-reading-sum-golang-org-lookup-93058a058dd8)
* [Build Go project with GitLab CI](https://blog.boatswain.io/post/build-go-project-with-gitlab-ci/)
* [GitLab CI/CD Examples ](https://docs.gitlab.com/ee/ci/examples/)
* [Automatic Semantic Versioning in GitLab CI](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/)
* [https://nvie.com/posts/a-successful-git-branching-model/](https://nvie.com/posts/a-successful-git-branching-model/)
* [Semantic Versioning](https://semver.org/)
* [Building a Golang Docker image](https://bitfieldconsulting.com/golang/docker-image)
* [based on this pipeline](https://gitlab02.uchile.cl/proyectos-dcc/fea-simple-02/fea-simple-core/-/blob/develop/.gitlab-ci.yml)
* [Unit Testing with Golang](https://golangdocs.com/golang-unit-testing)
* [Go Rutine](https://gobyexample.com/goroutines)
